# requirement
- Debian-like linux
- google-chrome installed
 
# setup

run the `./setup.sh` file,
 it will:
  - download the chrome driver for the current directory
  - create a python 3 virtual environment
  - install the requirements in this enviroment

# execution

Use the following commands:
```
source venv\bin\activate
nosetests test_case.py
``` 

Check nosetests parameters if any report is needed
