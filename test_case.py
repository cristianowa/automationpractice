"""

    1.  Acessar o site: www.automationpractice.com.
    2.  Escolha um produto qualquer na loja.
    3.  Adicione o produto escolhido ao carrinho.
    4.  Prossiga para o checkout.
    5.  Valide se o produto foi corretamente adicionado ao carrinho e prossiga caso esteja tudo certo.
    6.  Realize o cadastro do cliente preenchendo todos os campos obrigatórios dos formulários.
    7.  Valide se o endereço está correto e prossiga.
    8.  Aceite os termos de serviço e prossiga.
    9.  Valide o valor total da compra.
    10. Selecione um método de pagamento e prossiga.
    11. Confirme a compra e valide se foi finalizada com sucesso.

"""

import os
import unittest

from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
from faker import Faker
from random import randint, choice

faker = Faker()


class User:
    """
    Helper user class
    """
    def __init__(self):
        self.email = faker.email()
        self.first_name = faker.first_name()
        self.last_name = faker.last_name()
        self.password = faker.password()
        self.birth_year = randint(1950,2010)
        self.birth_month = choice(["Ja", "F", "M", "A", "May"])
        self.birth_day = randint(1, 28)  # sue 28 to skip validations
        self.company = faker.company()
        self.address1, self.address2 = faker.address().split("\n")
        self.city = faker.city()
        self.state = faker.state() # only US states in form, only US states in faker!
        self.phone = faker.phone_number().split("x")[0] # this site does not accept extension lines
        self.postcode = faker.postcode()

class SimplePurchase(unittest.TestCase):
    def setUp(self) -> None:
        self.user = User()
        self.driver = webdriver.Chrome(executable_path=os.path.abspath("chromedriver"))
        self.wait = WebDriverWait(self.driver, 10)

    def tearDown(self) -> None:
        self.driver.stop_client()

    def test_simple_purshase(self):
        self.driver.get("http://automationpractice.com")
        # first page

        products = self.driver.find_element_by_id("center_column")

        item_to_click = products.find_element_by_tag_name("ul")

        # I think it is needed in higher resolutions
        hover = ActionChains(self.driver).move_to_element(item_to_click)
        hover.perform()
        
        item = self.driver.find_element_by_id("homefeatured").find_element_by_tag_name("li")
        
        product_name = item.find_element_by_tag_name("h5").find_element_by_tag_name("a").text
        price = float(item.text.split("\n")[1].replace("$", ""))  # improve this parser if price format changes often

        item.find_element_by_class_name("button").click()

        # try:
        # # second page
        #     next = self.driver.find_element_by_id("add_to_cart")
        #     next.find_element_by_class_name("exclusive").click()
        # except NoSuchElementException:
        #     pass #goes direct to checkout
        
        self.wait.until(EC.visibility_of_element_located((By.CLASS_NAME, "button-container")))
        
        self.driver.find_element_by_class_name("button-container").find_element_by_tag_name("a").click()

        cart = self.driver.find_element_by_class_name("cart_item")
        
        product_name_in_cart = cart.find_element_by_class_name("product-name").find_element_by_tag_name("a").text
        
        self.assertEqual(product_name_in_cart, product_name)
        
        cart_navigation = self.driver.find_element_by_class_name("cart_navigation")
        cart_navigation.find_element_by_class_name("button").find_element_by_tag_name("span").click()
        
        from IPython import embed
        
        # sign in
        self.driver.find_element_by_id("email_create").send_keys(self.user.email)
        account_form = self.driver.find_element_by_id("create-account_form")
        
        account_form.find_element_by_id("SubmitCreate").click()
        
        self.wait.until(EC.visibility_of_element_located((By.CLASS_NAME, "account_creation")))
        # fill
        account_creation = self.driver.find_element_by_class_name("account_creation")
        gender1 = account_creation.find_element_by_id("id_gender1")
        gender1.click()
        customer_firstname = account_creation.find_element_by_id("customer_firstname")
        customer_firstname.clear()
        customer_firstname.send_keys(self.user.first_name)
        customer_lastname = account_creation.find_element_by_id("customer_lastname")
        customer_lastname.clear()
        customer_lastname.send_keys(self.user.last_name)
        passwd = account_creation.find_element_by_id("passwd")
        passwd.send_keys(self.user.password)
        days = account_creation.find_element_by_id("days")
        days.send_keys(str(self.user.birth_day))
        months = account_creation.find_element_by_id("months")
        months.send_keys(str(self.user.birth_month))
        years = account_creation.find_element_by_id("years")
        years.send_keys(str(self.user.birth_year))
        
        
        address_creation = self.driver.find_elements_by_class_name("account_creation")[1]
        customer_firstname = address_creation.find_element_by_id("firstname")
        customer_firstname.clear()
        customer_firstname.send_keys(self.user.first_name)
        
        customer_lastname = address_creation.find_element_by_id("lastname")
        customer_lastname.clear()
        customer_lastname.send_keys(self.user.last_name)
        company = address_creation.find_element_by_id("company")
        company.send_keys(self.user.company)
        address1 = address_creation.find_element_by_id("address1")
        address1.send_keys(self.user.address1)
        address2 = address_creation.find_element_by_id("address2")
        address2.send_keys(self.user.address2)
        city = address_creation.find_element_by_id("city")
        city.send_keys(self.user.city)
        state = address_creation.find_element_by_id("id_state")
        state.send_keys(self.user.state)
        postcode = address_creation.find_element_by_id("postcode")
        postcode.send_keys(self.user.postcode)
        country = address_creation.find_element_by_id("id_country")
        country.send_keys("u")
        phone = address_creation.find_element_by_id("phone")
        phone.send_keys(self.user.phone)
        
        phone_mobile = address_creation.find_element_by_id("phone_mobile")
        phone_mobile.send_keys(self.user.phone)
        
        alias = address_creation.find_element_by_id("alias")
        alias.clear()
        alias.send_keys("home")
        self.driver.find_element_by_id("submitAccount").click()
        
        
        # check delivery address
        address_delivery = self.driver.find_element_by_id("address_delivery")
        address_firstname = address_delivery.find_element_by_class_name("address_firstname")
        self.assertIn(self.user.first_name, address_firstname.text)
        self.assertIn(self.user.last_name, address_firstname.text)
        address_company = address_delivery.find_element_by_class_name("address_company")
        print("address_company", address_company.text == self.user.company)
        address_address1 = address_delivery.find_element_by_class_name("address_address1")
        self.assertIn(self.user.address1, address_address1.text)
        self.assertIn(self.user.address2, address_address1.text)
        address_city_address_state_name = address_delivery.find_element_by_class_name("address_city")
        self.assertIn(self.user.city, address_city_address_state_name.text)
        self.assertIn(self.user.state, address_city_address_state_name.text)
        self.assertIn(self.user.postcode, address_city_address_state_name.text)
        address_phone = address_delivery.find_element_by_class_name("address_phone")
        self.assertEqual(address_phone.text, self.user.phone)
        address_phone_mobile = address_delivery.find_element_by_class_name("address_phone_mobile")
        self.assertEqual(address_phone_mobile.text, self.user.phone)
        
        # check billing address
        address_invoice = self.driver.find_element_by_id("address_invoice")
        address_firstname = address_invoice.find_element_by_class_name("address_firstname")
        self.assertIn(self.user.first_name, address_firstname.text)
        self.assertIn(self.user.last_name, address_firstname.text)
        address_company = address_invoice.find_element_by_class_name("address_company")
        self.assertEqual(address_company.text, self.user.company)
        address_address1 = address_invoice.find_element_by_class_name("address_address1")
        self.assertIn(self.user.address1, address_address1.text)
        self.assertIn(self.user.address2, address_address1.text)
        address_city_address_state_name = address_invoice.find_element_by_class_name("address_city")
        self.assertIn(self.user.city, address_city_address_state_name.text)
        self.assertIn(self.user.state, address_city_address_state_name.text)
        self.assertIn(self.user.postcode, address_city_address_state_name.text)
        address_phone = address_invoice.find_element_by_class_name("address_phone")
        self.assertEqual(address_phone.text, self.user.phone)
        address_phone_mobile = address_invoice.find_element_by_class_name("address_phone_mobile")
        self.assertEqual(address_phone_mobile.text, self.user.phone)
        
        self.driver.find_element_by_class_name("cart_navigation").find_element_by_tag_name("button").click()

        # accept terms
        self.wait.until(EC.visibility_of_element_located((By.CLASS_NAME, "checkbox")))
        self.driver.find_element_by_class_name("checkbox").find_element_by_tag_name("input").click()
        delivery_price = float(self.driver.find_element_by_class_name("delivery_option_price").text.replace("$", ""))
        
        self.driver.find_element_by_class_name("cart_navigation").find_element_by_tag_name("button").click()
        
        # validate price
        
        total_price = float(self.driver.find_element_by_id("total_price").text.replace("$", ""))
        self.assertEqual(total_price, delivery_price + price, "total price does not match")
        
        # pay by bankwire
        self.driver.find_element_by_class_name("bankwire").click()
        
        # confirm
        self.driver.find_element_by_class_name("cart_navigation").find_element_by_tag_name("button").click()
        
        order_confirmation = self.driver.find_element_by_class_name("columns-container").find_element_by_class_name("cheque-indent").find_element_by_tag_name("strong")
        
        self.assertIn("Your order on My Store is complete.", order_confirmation.text, "order not confirmed")


if __name__ == '__main__':
    unittest.main()