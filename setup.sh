#!/usr/bin/env bash
wget https://chromedriver.storage.googleapis.com/80.0.3987.106/chromedriver_linux64.zip
unzip chromedriver_linux64.zip
rm chromedriver_linux64.zip
virtualenv -p python3 venv
./venv/bin/pip install -r requirements.txt


